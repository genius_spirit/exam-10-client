import React, {Component, Fragment} from 'react';
import {Glyphicon, Image, PageHeader, Panel} from "react-bootstrap";
import {fetchNews, fetchRemoveNews} from "../../store/actions/actions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class News extends Component {

  componentDidMount() {
    this.props.onFetchNews();
  }

  render() {
    return(
      <Fragment>
        <PageHeader>News</PageHeader>
        {this.props.news.map((news, index) => (
          <Panel key={news.id}>
            <Panel.Heading>Post# {index}, Date: {news.date}</Panel.Heading>
            <Panel.Body>
              {news.image &&
              <Image style={{width: '200px', marginRight: '10px'}}
                     src={'http://localhost:8000/uploads/' + news.image} thumbnail
              />}
              <strong style={{marginLeft: '20px', fontSize: '20px'}}>
                {news.title}
              </strong>
            </Panel.Body>
            <Panel.Footer>
              <Link to={"/news/" + news.id}>Read full post</Link>
              <div style={{cssFloat: 'right', cursor: 'pointer'}} onClick={() => this.props.onFetchRemoveNews(news.id)}>
                Delete <Glyphicon glyph="remove" style={{fontSize: '16px', transform: 'translateY(3px)'}} />
              </div>
            </Panel.Footer>
          </Panel>
        ))}
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    news: state.news
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchNews: () => dispatch(fetchNews()),
    onFetchRemoveNews: id => dispatch(fetchRemoveNews(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(News);