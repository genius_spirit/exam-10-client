import React, {Component, Fragment} from 'react';
import { fetchComments, fetchFullNews, fetchRemoveComment } from "../../store/actions/actions";
import {connect} from "react-redux";
import { Image, ListGroup, ListGroupItem, PageHeader, Panel } from "react-bootstrap";
import AddCommentForm from "../../components/AddComment/AddCommentForm";

class FullNews extends Component {

  componentDidMount() {
    this.props.onFetchFullNews(this.props.match.params.id);
    this.props.onFetchComments(this.props.match.params.id);
  }

  getComments = id => {
    this.props.onFetchRemoveComment(id)
    .then(() => this.props.onFetchComments(this.props.match.params.id));
  };

  render() {
    return(
      <Fragment>
        {this.props.fullNews ?
          <Panel>
            <Panel.Heading style={{fontSize: '20px'}}>{this.props.fullNews[0].title}</Panel.Heading>
              <ListGroup>
                <ListGroupItem>
                  <Panel.Body>Date: {this.props.fullNews[0].date}</Panel.Body>
                  <Panel.Body>
                    {this.props.fullNews[0].image &&
                    <Image style={{width: '300px', textAlign: 'center'}}
                           src={'http://localhost:8000/uploads/' + this.props.fullNews[0].image} rounded />}
                  </Panel.Body>
                  <Panel.Body> {this.props.fullNews[0].content}</Panel.Body>
                </ListGroupItem>
                <ListGroupItem>
                  <Panel.Body>
                    <PageHeader><small>Comments</small></PageHeader>
                    {this.props.comments ?
                      this.props.comments.map((item, index) => (
                        <Panel key={index}>
                          <Panel.Body><strong style={{marginRight: '20px'}}>{item.author}</strong>
                            {item.comment}
                            <div style={{float: 'right', cursor: 'pointer'}} onClick={() => this.getComments(item.id)}>Delete</div>
                          </Panel.Body></Panel>
                      )) : null}
                  </Panel.Body>
                </ListGroupItem>
                <AddCommentForm newsId={this.props.match.params.id}/>
              </ListGroup>
          </Panel> : null
        }
      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    fullNews: state.fullNews,
    comments: state.comments
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchFullNews: id => dispatch(fetchFullNews(id)),
    onFetchComments: id => dispatch(fetchComments(id)),
    onFetchRemoveComment: id => dispatch(fetchRemoveComment(id))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FullNews);