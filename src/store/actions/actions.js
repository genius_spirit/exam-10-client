import axios from '../../axios-api';
import {FETCH_COMMENTS_SUCCESS, FETCH_FULL_NEWS_SUCCESS, FETCH_NEWS_SUCCESS} from "./actionTypes";

export const fetchProductsSuccess = data => {
  return {type: FETCH_NEWS_SUCCESS, data};
};

export const fetchNews = () => {
  return dispatch => {
    return axios.get('/news').then(
      response => dispatch(fetchProductsSuccess(response.data))
    )
  }
};

export const fetchFullNews = id => {
  return dispatch => {
    return axios.get(`/news/${id}`).then(
      response => dispatch(fetchFullNewsSuccess(response.data))
    )
  }
};

export const fetchFullNewsSuccess = data => {
  return {type: FETCH_FULL_NEWS_SUCCESS, data};
};

export const  fetchRemoveNews = id => {
  return dispatch => {
    return axios.delete(`/news/${id}`).then(() => dispatch(fetchNews())
  )}
};

export const fetchAddNews = data => {
  return dispatch => {
    return axios.post('/news', data).then(() => dispatch(fetchNews())
  )};
};

export const fetchComments = id => {
  return dispatch => {
    return axios.get(`/comments?news_id=${id}`).then(
      response => dispatch(fetchCommentsSuccess(response.data)))
  }
};

export const fetchCommentsSuccess = data => {
  return {type: FETCH_COMMENTS_SUCCESS, data};
};

export const fetchRemoveComment = id => {
  return dispatch => {
    return axios.delete(`/comments/${id}`);
  }
};

export const fetchAddComment = data => {
  return dispatch => {
    return axios.post('/comments', data).then(() => dispatch(fetchNews())
    )};
};