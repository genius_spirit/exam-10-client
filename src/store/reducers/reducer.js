import { FETCH_COMMENTS_SUCCESS, FETCH_FULL_NEWS_SUCCESS, FETCH_NEWS_SUCCESS } from "../actions/actionTypes";

const initialstate = {
  news: [],
  fullNews: null,
  comments: []
};

const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case FETCH_NEWS_SUCCESS:
      return {...state, news: action.data};
    case FETCH_FULL_NEWS_SUCCESS:
      return {...state, fullNews: action.data};
    case FETCH_COMMENTS_SUCCESS:
      return {...state, comments: action.data};
    default:
      return state;
  }
};

export default reducer;