import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Route, Switch} from "react-router-dom";
import News from "./containers/News/News";
import AddNewsForm from "./components/AddNews/AddNewsForm";
import FullNews from "./containers/FullNews/FullNews";


class App extends Component {
  render() {
    return (
      <Fragment>
        <header><Toolbar/></header>
        <div className="container">
          <Switch>
            <Route path="/" exact component={News}/>
            <Route path="/add-news" exact component={AddNewsForm}/>
            <Route path="/news/:id" exact component={FullNews}/>
          </Switch>
        </div>
      </Fragment>
    );
  }
}

export default App;