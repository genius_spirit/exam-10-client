import React, { Component, Fragment } from "react";
import {
  Button,
  Col,
  ControlLabel,
  Form,
  FormControl,
  FormGroup,
  ListGroupItem,
  PageHeader,
  Panel
} from "react-bootstrap";
import { fetchAddComment, fetchComments } from "../../store/actions/actions";
import { connect } from "react-redux";

class AddCommentForm extends Component {

  state = {
    author: '',
    comment: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = {newsId: this.props.newsId, author: this.state.author, comment: this.state.comment};
    console.log(formData);

    this.props.onFetchAddComment(formData).then(() => this.props.onFetchComments(this.props.newsId));
  };

  render() {
    return(
       <Fragment>
         <ListGroupItem>
           <Panel.Body>
             <PageHeader><small>Add Comment</small></PageHeader>
             <Form horizontal onSubmit={this.submitFormHandler}>
               <FormGroup controlId="commentName">
                 <Col componentClass={ControlLabel} sm={2}>Name</Col>
                 <Col sm={10}>
                   <FormControl
                     type="text"
                     placeholder="Enter name"
                     name="author"
                     value={this.state.author}
                     onChange={this.inputChangeHandler}
                   />
                 </Col>
               </FormGroup>
               <FormGroup controlId="commentTitle">
                 <Col componentClass={ControlLabel} sm={2}>Comment</Col>
                 <Col sm={10}>
                   <FormControl required
                     componentClass="textarea"
                     placeholder="Enter comment"
                     name="comment"
                     value={this.state.comment}
                     onChange={this.inputChangeHandler}
                   />
                 </Col>
               </FormGroup>
               <FormGroup>
                 <Col smOffset={2} sm={10}>
                   <Button bsStyle="primary" type="submit">Save</Button>
                 </Col>
               </FormGroup>
             </Form>
           </Panel.Body>
         </ListGroupItem>
       </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFetchAddComment: (data) => dispatch(fetchAddComment(data)),
    onFetchComments: id => dispatch(fetchComments(id))

  }
};

export default connect(null, mapDispatchToProps)(AddCommentForm);