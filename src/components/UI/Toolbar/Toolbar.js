import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = () => {
  return(
    <Navbar>
      <Navbar.Header>
        <Navbar.Brand>
          <LinkContainer to="/"><a>News</a></LinkContainer>
        </Navbar.Brand>
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav pullRight>
          <LinkContainer to="/add-news" exact>
            <NavItem>Add news</NavItem>
          </LinkContainer>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
};

export default Toolbar;