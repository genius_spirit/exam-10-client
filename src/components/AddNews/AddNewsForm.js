import React, {Component, Fragment} from 'react';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {PageHeader} from "react-bootstrap";
import {connect} from "react-redux";
import { fetchAddNews} from "../../store/actions/actions";

class AddNewsForm extends Component {

  state = {
    title: '',
    content: '',
    image: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onFetchAddNews(formData).then(this.props.history.push('/'));
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.author]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.author]: event.target.files[0]
    });
  };


  render() {
    return(
      <Fragment>
        <PageHeader>Add new post</PageHeader>
        <Form horizontal onSubmit={this.submitFormHandler}>
          <FormGroup controlId="newsTitle">
            <Col componentClass={ControlLabel} sm={2}>Title</Col>
            <Col sm={10}>
              <FormControl
                type="text"
                placeholder="Enter news title"
                name="title"
                value={this.state.title}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>
          <FormGroup controlId="productDescription">
            <Col componentClass={ControlLabel} sm={2}>Content</Col>
            <Col sm={10}>
              <FormControl required
                componentClass="textarea"
                placeholder="Enter text"
                name="content"
                value={this.state.description}
                onChange={this.inputChangeHandler}
              />
            </Col>
          </FormGroup>
          <FormGroup controlId="newsImage">
            <Col componentClass={ControlLabel} sm={2}>Image</Col>
            <Col sm={10}>
              <FormControl
                type="file"
                name="image"
                onChange={this.fileChangeHandler}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">Save</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onFetchAddNews: data => dispatch(fetchAddNews(data))
  }
};

export default connect(null, mapDispatchToProps)(AddNewsForm);
